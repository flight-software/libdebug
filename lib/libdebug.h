#ifndef _LIB_DEBUG_H
#define _LIB_DEBUG_H

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdio.h>

// Fuck stdint.h for being wrong - Dillon (01/02/2019)
#ifdef INT8_C
#undef INT8_C
#endif

#ifdef INT16_C
#undef INT16_C
#endif

#ifdef UINT8_C
#undef UINT8_C
#endif

#ifdef UINT16_C
#undef UINT16_C
#endif

#define INT8_C(c)   ((int8_t)(c))
#define INT16_C(c)  ((int16_t)(c))

#define UINT8_C(c)  ((uint8_t)(c))
#define UINT16_C(c) ((uint16_t)(c))
// End Fuck stdint.h

// ====================================================================
// USE THIS STUFF
#define CAST_TO_FUNC_PTR(func_ptr_var, void_ptr_var) \
_Pragma("GCC diagnostic push") \
_Pragma("GCC diagnostic ignored \"-Wpedantic\"") \
    func_ptr_var = (typeof(func_ptr_var)) ((void*)void_ptr_var); \
_Pragma("GCC diagnostic pop")

#define CAST_FUNC_TO_VOID_PTR(void_ptr_var, func_ptr_var) \
_Pragma("GCC diagnostic push") \
_Pragma("GCC diagnostic ignored \"-Wpedantic\"") \
    void_ptr_var = (void*)(func_ptr_var); \
_Pragma("GCC diagnostic pop")

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#ifdef DEBUG
#define P_DEBUG(x___, ...) \
    syslog(LOG_DEBUG, "(%s)[%d]" x___, __FILE__, __LINE__, __VA_ARGS__); \
    printf(ANSI_COLOR_MAGENTA "DEBUG(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__, __VA_ARGS__);

#define P_DEBUG_STR(x___) \
    syslog(LOG_DEBUG, "(%s)[%d]" x___, __FILE__, __LINE__); \
    printf(ANSI_COLOR_MAGENTA "DEBUG(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__);

#define P_INFO(x___, ...) \
    syslog(LOG_INFO, "(%s)[%d]" x___, __FILE__, __LINE__, __VA_ARGS__); \
    printf(ANSI_COLOR_GREEN "INFO(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__, __VA_ARGS__);

#define P_INFO_STR(x___)  \
    syslog(LOG_INFO, "(%s)[%d]" x___, __FILE__, __LINE__); \
    printf(ANSI_COLOR_GREEN "INFO(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__);
#else
#define P_DEBUG(...) ;
#define P_DEBUG_STR(...) ;
#define P_INFO(...) ;
#define P_INFO_STR(...) ;
#endif

#define P_ERR(x___, ...) \
    syslog(LOG_ERR, "(%s)[%d]" x___, __FILE__, __LINE__, __VA_ARGS__); \
    printf(ANSI_COLOR_RED "ERROR(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__, __VA_ARGS__);

#define P_ERR_STR(x___) \
    syslog(LOG_ERR, "(%s)[%d]" x___, __FILE__, __LINE__); \
    printf(ANSI_COLOR_RED "ERROR(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__);

#define P_LOGIC(x___, ...) \
    syslog(LOG_INFO, "LOGIC(%s)[%d]:\t" x___, __FILE__, __LINE__, __VA_ARGS__); \
    printf(ANSI_COLOR_BLUE "LOGIC(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__, __VA_ARGS__);

#define P_LOGIC_STR(x___) \
    syslog(LOG_INFO, "LOGIC(%s)[%d]:\t" x___, __FILE__, __LINE__); \
    printf(ANSI_COLOR_BLUE "LOGIC(%s)[%d]:\t" x___ ANSI_COLOR_RESET "\n", __FILE__, __LINE__);  
// USE THE PREVIOUS MACROS!!
// ====================================================================

#define MIN_BYTES_PER_LINE 4
#define DEFAULT_BYTE_DELIM ' '
#define MAX_BACKTRACE 256
extern int libdebug_print_ascii;


/*this will easily log all your integers up to 64 bit types. like so:
 *  time_t t = time(NULL);
 *	uint64_t thing = 0xFFFFFFFFFFFFFFFF;
 *	LOG_VARS(t,thing);
 *
 * output : 
 *               t: 595ce74e
 *           thing: ffffffffffffffff
 */
#define LOG_INTS(...) logInts(__FUNCTION__, __LINE__, #__VA_ARGS__, __VA_ARGS__)
//this will easily log a struct, string, or array
#define LOG_BUF(var) logBuf(#var, (uint8_t *)&var, sizeof(var), __FUNCTION__, __LINE__)
extern void logFloats(const char *func, int line, const char * varNames, ...);
extern void logInts(const char *func, int line, const char * varNames, ...);
extern void logBuf(const char * varName, uint8_t * var, size_t size
        , const char * func, int line);
extern void logBufMultiple(uint8_t * var, size_t offset, size_t len);
extern void print_struct(unsigned char * p, size_t length, int bytes_per_line);
extern int membeef(uint8_t*, uint8_t*, size_t);
extern int logBacktrace(void);
#define console_log( stream, format, ... ) \
        do { fprintf(stream, "%s: %s[%d]: " format, __FILE__, __func__, __LINE__, ##__VA_ARGS__); } while(0);

#ifdef DEBUG
#define debug_log( ... ) console_log(__VA_ARGS__)
#else
#define debug_log( ... )
#endif

#ifdef DEBUG
#define vod_log( STATEMENT , ... ) console_log(__VA_ARGS__)
#else
#define vod_log( STATEMENT, ... )  do { if(STATEMENT) { fprintf(__VA_ARGS__); } } while(0);
#endif

#define PRINT_FLOAT(var) \
    do { printf(#var ": %g\n", (var)); } while(0);

// Disable printf, etc...
#ifndef DEBUG
    #undef stdin
    #undef stdout
    #undef stderr
    #undef fprintf
    #undef printf
    #undef puts
#endif

#endif // libdebug.h

