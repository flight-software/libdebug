//=======================================================================================================================
// Written by LAICE software team in Oct, 2016, to give useful debugging functions

//=========================================================================================================================
#include <execinfo.h> /* backtrace */
#include <stdio.h>
#include <syslog.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include "libdebug.h"

char byte_delim = DEFAULT_BYTE_DELIM;
int libdebug_print_ascii = 0;

/**
 * Appends to the syslog the zero-padded bytes of var in the
 * interval [offset, offset + len), eg not including the
 * uppermost byte.
 *
 * These bytes appear as one line in the syslog.
 *
 * TODO: replace the calloc with a appropriately sized char
 * buffer, as snprintf() always adds the NUL byte.
 */
void logBufMultiple(uint8_t * var, size_t offset, size_t len)
{
    const char byte_format[] = "%02x ";
    size_t byte_len = 3; //strlen(byte_format);
    
    char * log_string = (char*)calloc(byte_len * len + 1, sizeof(char));
    for(size_t i = 0; i < len; i++) {
        snprintf(&log_string[i * byte_len], byte_len + 1
                , byte_format, var[offset + i]);
    }
    syslog(LOG_DEBUG, "Bytes [%04zu - %04zu] : %s", offset, offset + len - 1
            , log_string);
    free(log_string);
}

void logInts(const char *func, int line, const char * varNames, ...)
{
	char *dupVarNames;
	char * toFree;
	char *name;
	uint64_t arg;
	va_list ap;
	
	size_t numVars = 1; //this assumes that you call the function with an argument.
	for(size_t i = 0;  varNames[i] != '\0'; i++){
		if(varNames[i] == ','){
			numVars++;
		}
	}

	dupVarNames = strdup(varNames);
	toFree = dupVarNames;
	
	va_start(ap, varNames);
	for(size_t i = 0; i < numVars; i++){
		name = strsep(&dupVarNames, ",");
		arg = va_arg(ap, uint64_t);
		//printf("[%s:%d] %s: %0llx\n", func, line, name, arg);
		syslog(LOG_DEBUG, "[%s:%d] %s: %0llx\n", func, line, name, arg);
	}
	
	va_end(ap);
	free(toFree);
}

void logFloats(const char *func, int line, const char * varNames, ...)
{
	char *dupVarNames;
	char * toFree;
	char *name;
	double arg;
	va_list ap;
	
	size_t numVars = 1; //this assumes that you call the function with an argument.
	for(int i = 0;  varNames[i] != '\0'; i++){
		if(varNames[i] == ','){
			numVars++;
		}
	}

	dupVarNames = strdup(varNames);
	toFree = dupVarNames;
	
	va_start(ap, varNames);
	for(size_t i = 0; i < numVars; i++){
		name = strsep(&dupVarNames, ",");
		arg = va_arg(ap, double); //double is 64 bits
		printf("[%s:%d] %s: %f\n", func, line, name, arg);
		syslog(LOG_DEBUG, "[%s:%d] %s: %f\n", func, line, name, arg);
	}
	
	va_end(ap);
	free(toFree);
}

void logBuf(const char * varName, uint8_t * var, size_t size
        , const char * func, int line)
{
	syslog(LOG_DEBUG, "--- START BUFFER LOG (%s:%d [%s]) --- ", func, line, varName);
    size_t bytes_per_line = MIN_BYTES_PER_LINE;
    // Compute the ceiling of the number of bytes / bytes per line
    size_t numRows = (size / bytes_per_line) + (size % bytes_per_line > 0);
    
    // compute how many bytes should appear on the last line.
    // If it's evenly divisible, it's the normal number of bytes per line.
    // otherwise, it's the remainder mod bytes_per_line
    size_t lastLineLength = size % bytes_per_line;
    lastLineLength = lastLineLength ? lastLineLength : bytes_per_line;
    
    for(size_t i = 0; i < numRows; i++) {
        size_t offset = i * bytes_per_line;
        bytes_per_line = (i+1 == numRows) ? lastLineLength : bytes_per_line;
        logBufMultiple(var, offset, bytes_per_line);
    }
	syslog(LOG_DEBUG, "--- END BUFFER LOG ---");
}

void print_struct(unsigned char *p, size_t length, int bytes_per_line)
{
#ifdef DEBUG
    static size_t i = 0;
    size_t modulus = (size_t)((bytes_per_line > MIN_BYTES_PER_LINE) \
                        ? bytes_per_line : MIN_BYTES_PER_LINE);
    for(i = 0; i < length; i++)
    {
        // Every "modulus" lines (excluding 0) print a newline.
        if(i && !(i % modulus) && i + 1 <= length) 
            fprintf(stderr, "\n");
        if(libdebug_print_ascii && isprint(p[i]) && 
            p[i] != '\n') {
            fprintf(stderr, /*"%02x "*/" %c%c", p[i]
                            , byte_delim);
        } else {
            fprintf(stderr, "%02x%c", p[i], byte_delim);
        }
    }
    // Throw in an extra newline to avoid colliding with other messages.
    fprintf(stderr, "\n");
#else
    (void)p; (void)length; (void)bytes_per_line;
#endif
    return;

}

int membeef(uint8_t * dest, uint8_t * src, size_t len) 
{
    // Dropin replacement for memcpy, ignoring src and using beef.
    (void)src;
    for(size_t i = 0; i < len; dest[i] = i % 2 ? 0xef : 0xbe, i++);
    return EXIT_SUCCESS;
}

int logBacktrace(void)
{
    int i, nptrs;
    void * backtrace_buffer[MAX_BACKTRACE];
    char ** callstack;

    nptrs = backtrace(backtrace_buffer, MAX_BACKTRACE);

    callstack = backtrace_symbols(backtrace_buffer, nptrs);

    if(callstack == NULL) {
        syslog(LOG_ERR,"Unable to generate callstack, errors: %s", strerror(errno));
        return EXIT_FAILURE;
    }

    // Don't include this function
    syslog(LOG_INFO, "Callstack/Backtrace requested (%d addresses):", nptrs-1);
    for(i = 1; i < nptrs; i++) {
        syslog(LOG_INFO, "%s", callstack[i]);
    }
    free(callstack);

    return EXIT_SUCCESS;    
}
